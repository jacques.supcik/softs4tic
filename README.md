# Softs4TIC

<https://softs4tic.tic.heia-fr.ch/>

I've developed and tested this page with the [official docker image](https://hub.docker.com/r/squidfunk/mkdocs-material/).

You can download the "public" directory from

<https://gitlab.forge.hefr.ch/jacques.supcik/softs4tic/-/jobs/artifacts/master/download?job=pages>