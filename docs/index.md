# TIC - Installation des Logiciels de Base

## Introduction

Ce site vous explique comment installer les logiciels qui seront utilisés dans le cadre de certains cours en filières Informatique et Télécommunications.

Nous vous invitons à installer ces logiciels en suivant les indications ci-après. Cela vous permettra ainsi de gagner du temps et de pouvoir utiliser plus rapidement ces logiciels lors des travaux pratiques (laboratoires).

Si vous ne vous sentez pas apte à effectuer ces installations ou si vous rencontrez des problèmes, une assistance sera proposée lors de la première séance de laboratoire.

Les instructions d'installation sont données pour le système d'exploitation Windows et Mac OSX. Pour Linux, les étudiants se chargeront eux-mêmes de trouver et d'installer les logiciels correspondants (ils existent). La phase de configuration d'Intellij est la même pour tous les systèmes.

Les installations doivent être effectuées dans l'ordre proposé ci-dessous.

## Installation de Java (Optionnel)

Pour les cours de programmation, nous utiliserons la version 11 de Java. Pour compiler les programmes que vous allez écrire,
vous aurez besoin du JDK (Java Development Kit). La version 11 du JDK est déjà incluse dans la dernière version d'IntelliJ IDEA
que vous téléchargerez au point suivant. Si vous souhaitez une autre version du JDK vous avez plusieurs options. Voici les options
les plus populaires: 

* [Java SE Development Kit 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)
* [Amazon Corretto 11](https://docs.aws.amazon.com/fr_fr/corretto/latest/corretto-11-ug/downloads-list.html)
* [AdoptOpenJDK](https://adoptopenjdk.net/)

## Installation et Configuration de IntelliJ IDEA

* Pour les cours de première année, nous utiliserons le logiciel IntelliJ IDEA de [Jetbrains](https://www.jetbrains.com/). Vous pouvez n'installer ce logiciel directement si vous voulez, mais pour simplifier les mises à jour, nous vous proposons d'installer le «toolbox». [^1]
* Rendez-vous sur le site de Jet Brain pour [télécharger la version du toolbox](https://www.jetbrains.com/toolbox/app/) qui correspond à votre système d'exploitation

![toolbox](images/web02.png)

* Installez et démarrez le logiciel en laissant les paramètres par défaut. Vous n'avez probablement pas
d'ancienne configuration et vous pouvez alors choisir «Do not import settings»

![toolbox](images/settings.png)

* Le Toolbox est accessible dans la barre des tâches de votre système. Cliquez sur l'icône du «toolbox» et installez «IntelliJ IDEA Community»

![toolbox](images/install.png)

* Dès qu’«IntelliJ IDEA Community» sera installé, il apparaîtra tout en haut de la liste des outils.
* Cliquez sur «IntelliJ IDEA Community» pour démarrer l'application

![intelliJ](images/welcome.png)

* Créez un nouveau projet (Create New Project) Le «Project SDK» devrait être configuré avec le SDK
interne d'IntelliJ et devrait afficher la version 11. Si vous avez téléchargé un autre SDK, c'est
ici que vous pouvez le choisir pour ce nouveau projet.

![intelliJ](images/new-project-0.png)

* Cliquez sur «Next». Le système vous propose ensuite d'utiliser un «template». Ne sélectionnez rien et cliquez sur «Next»
* Nommez le projet «PR1» et choisissez le répertoire qui sera utilisé pour le projet (le répertoire proposé par défaut fera probablement bien l'affaire)

![intelliJ](images/new-project-1.png)

* Si vous avez une machine Windows, il est probable qu'IntelliJ vous propose de reconfigurer «Windows Defender»
pour améliorer les performances

![intelliJ](images/new-project-2.png)

* Cliquez sur «fix...» et laissez IntelliJ configurer automatiquement «Windows Defender».
* Après quelques secondes, vous serez prêt à écrire votre premier programme. Dans le panneau de gauche, cliquez sur «PR1» pour ouvrir le projet, puis cliquez avec le bouton de droite de la souris sur «src» et sélectionnez «New --> Package». Nommez votre package «intro».

![intelliJ](images/new-package.png)

* Cliquez sur «intro», puis, avec le bouton de droite de la souris, sélectionnez «New --> Java Class». Nommez votre classe «hello»
* Entrez le code suivant :


``` java
package intro;

public class hello {
    public static void main(String[] args) {
        System.out.println("Hello Java!");
    }
}
```

Cliquez sur le triangle vert à gauche de «public static ...» et vous devrez voir le résultat suivant:

![hello](images/code.png)

Voilà, vous êtes maintenant prêt pour faire les premiers travaux pratiques. Lisez encore le [tutoriel](https://www.jetbrains.com/help/idea/creating-and-running-your-first-java-application.html) sur le site de Jetbrains pour avoir plus d'information sur la création de votre premier programme Java.

!!! note
    Si vous avez des questions, des remarques, des suggestions, ou si vous voyez des des erreurs sur cette page, n'hésitez pas à m'envoyer un e-mail à l'adresse <jacques.supcik@hefr.ch>.

[^1]: Quand vous aurez votre compte «edu.hefr.ch», vous pourrez vous inscrire et profiter **gratuitement** de tous les logiciels de Jetbrains.